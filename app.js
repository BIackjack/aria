const Discord = require('discord.js');
const Auth = require('./auth.json');

const BotMethods = require('./bot-methods');

const bot = new Discord.Client();

bot.login(Auth.token);

bot.on('ready', () => BotMethods.onReady(bot));
bot.on('message', BotMethods.handleMessage);
