const fs = require('fs');
const path = require('path');
const deburr = require('lodash.deburr');

const Generals = require('./generals.json');
const Constants = require('./constants');

const filePath = path.join(__dirname, Generals.listPath)
const words = fs.readFileSync(filePath, 'utf8').split('\n').map(line => line.trim())
const wordsWithoutAccents = words.map(word => deburr(word))

exports.getDefaultGameState = () => ({
    isRunning: false,
    mysteryWord: undefined,
    indications: []
})

exports.getNewGameState = () => {
    const randomIndex = Math.floor(Math.random() * words.length);

    return {
        isRunning: true,
        mysteryWord: words[randomIndex],
        mysteryWordIndex: randomIndex,
        indications: [words[0], words[words.length - 1]],
        indicationsIndex: [0, words.length - 1]
    }
}

exports.estimateWord = (submittedWord, gameState) => {
    let submittedWordIndex = words.indexOf(submittedWord)

    // Si on ne trouve pas de correspondance parfaite on réessaye quand même sans les accents
    if (submittedWordIndex < 0) {
        const submittedWordNoAccentsIndex = wordsWithoutAccents.indexOf(deburr(submittedWord))
        if (submittedWordNoAccentsIndex < 0) {
            const isAWord = RegExp(/^[A-z-]*$/, 'i').test(deburr(submittedWord));
            return isAWord ? Constants.CHECK_TYPES.WRONG_GUESS : Constants.CHECK_TYPES.INVALID_GUESS;
        }
        submittedWordIndex = submittedWordNoAccentsIndex
    }

    // Si les mecs sont vraiment nuls et ont répondu en dehors des bornes
    if (submittedWordIndex < gameState.indicationsIndex[0] || submittedWordIndex > gameState.indicationsIndex[1]) {
        return Constants.CHECK_TYPES.WRONG_GUESS;
    }

    if (submittedWordIndex >= gameState.mysteryWordIndex) {
        // Si on réduit l'intervalle par la fin
        gameState.indications[1] = words[submittedWordIndex]
        gameState.indicationsIndex[1] = submittedWordIndex
    }

    if (submittedWordIndex <= gameState.mysteryWordIndex) {
        // Si on réduit l'intervalle par le début
        gameState.indications[0] = words[submittedWordIndex]
        gameState.indicationsIndex[0] = submittedWordIndex
    }

    if (gameState.indicationsIndex[0] === gameState.indicationsIndex[1]) {
        return Constants.CHECK_TYPES.CORRECT_GUESS;
    }

    return Constants.CHECK_TYPES.VALID_GUESS;
}
