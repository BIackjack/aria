const Discord = require('discord.js')
const Constants = require('./constants')
const Generals = require('./generals.json')
const Words = require('./words.js')

let allGames = {};

const onReady = bot => {
    console.log("I'm ready, I'm a lady!")
    bot.user.setActivity('people type $help', { type: "WATCHING" })
        .then(presence => console.log(`Activity set to ${presence.activities[0].name} nyaa~~`))
        .catch(console.error);
};

const handleMessage = message => {
    const { channel, author } = message;
    let gameState = allGames[channel.id] || Words.getDefaultGameState();

    if (message.content[0] === Generals.charBeforeCommand) {
        const shiftedMessage = message.content.substring(1)
        const words = shiftedMessage.split(' ')
        const command = words.shift()

        if (command === Generals.commands.newGame && !gameState.isRunning) {
            // Commence une partie
            gameState = Words.getNewGameState()
            console.log(`I have chosen "${gameState.mysteryWord}" for channel #${channel.name} [${channel.id}] as secret word nyaa~~`)
            const embed = new Discord.RichEmbed()
                .setTitle(`${gameState.indications[0]} 🡒 ${gameState.indications[1]}`)
                .setDescription(`Nouvelle partie initiée par ${author.username} nyaa~~`)
                .setColor('#6ed169')
                .setFooter('(=´ᆺ｀=)', author.avatarURL)
            channel.send({ embed })
        }

        if (command === Generals.commands.stopGame && gameState.isRunning) {
            // Interrompt une partie
            const embed = new Discord.RichEmbed()
                .setTitle(`La partie a été interrompue par ${author.username}`)
                .setDescription(`La réponse attendue était **${gameState.mysteryWord}** nyaa~~`)
                .setColor('#fc6f65')
                .setFooter('(=´ᆺ｀=)', author.avatarURL)
            gameState = Words.getDefaultGameState();
            channel.send({ embed })
        }

        if (command === Generals.commands.botHelp) {
            const embed = new Discord.RichEmbed()
                .setTitle('Commandes')
                .setDescription('Trouvez le mot auquel je pense en procédant par dichotomie nyaa~~')
                .addField('Nouvelle partie', '`$new`')
                .addField('Interrompre la partie', '`$end`')
                .addField('Statut de la partie', '`$info`')
                .addField("Ouvrir l'aide", '`$help`')
                .setColor('#6ed169')
                .setFooter('(=´ᆺ｀=)')
            channel.send({ embed })
        }

        if (command === Generals.commands.gameInfo) {
            // Informe sur l'état de la partie en cours
            if (gameState.isRunning) {
                const embed = new Discord.RichEmbed()
                    .setTitle(`${gameState.indications[0]} 🡒 ${gameState.indications[1]}`)
                    .setDescription('La partie est en cours nyaa~~')
                    .setColor('#6ed169')
                    .setFooter('(=´ᆺ｀=)')
                channel.send({ embed })
            } else {
                const embed = new Discord.RichEmbed()
                    .setTitle('Aucune partie en cours')
                    .setDescription('Créez-en une avec la commande `$new` nyaa~~')
                    .setColor('#6ed169')
                    .setFooter('(=´ᆺ｀=)')
                channel.send({ embed })
            }
        }
    } else if (gameState.isRunning && !author.bot) {
        const splitMessage = message.content.split(' ').filter(word => !!word)
        if (splitMessage.length !== 1) return undefined;

        // Proposition de réponse par un joueur
        const submittedWord = splitMessage[0]
        const wordCheck = Words.estimateWord(submittedWord, gameState)

        if (wordCheck === Constants.CHECK_TYPES.WRONG_GUESS) {
            channel.send(`**${submittedWord}** ? Ce n'est pas une proposition valide nyaa~~`)
        } else if (wordCheck === Constants.CHECK_TYPES.CORRECT_GUESS) {
            // Si les 2 indications sont identiques, le mot mystère a été trouvé donc on termine la partie
            // On ne se base pas sur le submittedWord pour pouvoir valider les mots sans les accents
            const embed = new Discord.RichEmbed()
                .setTitle(`${author.username} a trouvé le mot "${gameState.mysteryWord}" !`)
                .setDescription('La partie est terminée nyaa~~')
                .setThumbnail('https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/120/twitter/248/party-popper_1f389.png')
                .setColor('#6ed169')
                .setFooter('(=´ᆺ｀=)')
            channel.send({ embed })

            gameState = Words.getDefaultGameState();
        } else if (wordCheck === Constants.CHECK_TYPES.VALID_GUESS) {
            // Sinon on continue la partie
            console.log(`${author} submitted the word in channel #${channel.name} [${channel.id}]: ${submittedWord} | New indications: [${gameState.indications[0]} - ${gameState.indications[1]}]`)

            const embed = new Discord.RichEmbed()
                .setTitle(`${gameState.indications[0]} 🡒 ${gameState.indications[1]}`)
                .setColor('#85c4ff')
                .setFooter('(=´ᆺ｀=)', author.avatarURL)
            channel.send({ embed })
        }
    }

    // On rafraichit les données de la partie pour ce channel
    allGames[channel.id] = gameState;
};

exports.onReady = onReady;
exports.handleMessage = handleMessage;
